package config

import (
	"fmt"
	"gitlab.com/golight/boilerplate/utils"
	"go.uber.org/zap"
	"os"
	"reflect"
	"strconv"
)

type AppConf struct {
	AppName   string       `env:"APP_NAME" yaml:"app_name"`
	Server    utils.Server `yaml:"server"`
	Logger    *zap.Logger  `yaml:"logger"`
	DB        utils.DB     `yaml:"db"`
	WorkerApi string       `env:"WORKER_API_KEY" yaml:"worker_api"`
}

func NewAppConf() AppConf {
	var config AppConf
	err := Init(&config)
	if err != nil {
		fmt.Printf("Error mapping env to struct: %v\n", err)
		return AppConf{}
	}
	return config
}

func Init(config interface{}) error {
	// используем пакет reflect для получения информации о типе и значении переменной config
	configValue := reflect.ValueOf(config)

	// если переданное значение является указателем, разыменовываем его
	if configValue.Kind() == reflect.Ptr {
		configValue = configValue.Elem()
	}

	// перебираем все поля структуры
	for i := 0; i < configValue.NumField(); i++ {
		fieldValue := configValue.Field(i)
		fieldType := configValue.Type().Field(i)

		// если поле является структурой, рекурсивно вызываем функцию Init
		if fieldValue.Kind() == reflect.Struct {
			if err := Init(fieldValue.Addr().Interface()); err != nil {
				return err
			}
			continue
		}

		// получаем значение тега "env" для поля
		tagValue := fieldType.Tag.Get("env")
		if tagValue == "" {
			continue
		}

		// получаем значение переменной окружения
		envValue := os.Getenv(tagValue)
		if envValue == "" {
			continue
		}

		// в зависимости от типа поля устанавливаем его значение
		switch fieldValue.Kind() {
		case reflect.String:
			fieldValue.SetString(envValue)
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			intValue, err := strconv.ParseInt(envValue, 10, 64)
			if err != nil {
				return fmt.Errorf("error parsing int value for field %s: %v", fieldType.Name, err)
			}
			fieldValue.SetInt(intValue)
		default:
			return fmt.Errorf("unsupported field type for field %s", fieldType.Name)
		}
	}
	return nil
}
