package worker

import (
	"binance-parser/internal/models"
	"binance-parser/internal/storage"
	"context"
	"encoding/json"
	"fmt"
	"go.uber.org/zap"
	"net/http"
	"strconv"
	"time"
)

// Основные константы для запроса к API Binance
const (
	BinanceAPIURL      = "https://api.binance.com/api/v1/klines"
	ExchangeInfoAPIURL = "https://api.binance.com/api/v1/exchangeInfo"
	Interval           = "1d" // Интервал времени для свечей (1 день)
)

type Worker struct {
	logger    *zap.Logger
	ApiKey    string
	IsRunning bool
	StopChan  chan bool
	Storage   *storage.Storage
}

func NewWorker(logger *zap.Logger, storage *storage.Storage, apiKey string) *Worker {
	return &Worker{logger: logger, Storage: storage, ApiKey: apiKey}
}

func (w *Worker) Run(ctx context.Context, stopChan chan bool) error {
	w.logger.Info("worker started")

	// получение всех пар торгов
	pairs, err := w.fetchAllPairs()
	if err != nil {
		w.logger.Error("fetchAllPairs error", zap.Error(err))
		return err
	}
	// перебираем пары, для каждой пасим все значения
	for _, pair := range pairs {

		// механизм остановки парсинга
		select {
		case <-stopChan:
			w.logger.Info("Stop parsing")
			return nil
			// если нет сигнала из канала, код продолжает выполнение
		default:
		}

		startTime := time.Time{} // начальное время для получения свечей

		// получение свечей для пары
		klines, err := w.fetchKLines(pair, startTime)
		if err != nil {
			w.logger.Error("get api error", zap.Error(err))
			continue
		}
		// данные выбраны, переходим к следующей паре
		if len(klines) == 0 {
			continue
		}
		// обновление времени начала для следующего запроса
		startTime = klines[len(klines)-1].OpenTime.Add(24 * time.Hour)

		// сохранение свечей в репозитории
		err = w.Storage.Save(ctx, klines)
		if err != nil {
			w.logger.Error("update error", zap.Error(err))
		}
	}
	w.logger.Info("worker finished")
	return nil
}

// тип для сырых данных (массив массивов) с Binance API
type rawKLine []interface{}

// получаем свечи для заданной пары и времени начала
func (w *Worker) fetchKLines(pair string, startTime time.Time) ([]models.KLine, error) {
	// формирование URL запроса
	url := fmt.Sprintf("%s?symbol=%s&interval=%s&limit=1000", BinanceAPIURL, pair, Interval)

	// если startTime установлено и не является нулевым, к исходной строке URL добавляется параметр начального времени
	if !startTime.IsZero() {
		url = fmt.Sprintf("%s&startTime=%d", url, startTime.UnixNano()/int64(time.Millisecond))
	}
	rawKlines := make([]rawKLine, 0) // сырые данные свечей

	// делаем запрос к Binance API
	if err := w.makeAPIRequest(url, &rawKlines); err != nil {
		return nil, err
	}

	// преобразование сырых данных в структуры модели
	klines := make([]models.KLine, 0, len(rawKlines))
	for _, rk := range rawKlines {
		openTimeUnix := int64(rk[0].(float64)) / 1000
		closeTimeUnix := int64(rk[6].(float64)) / 1000

		kline := models.KLine{
			Name:                pair,
			OpenTime:            time.Unix(openTimeUnix, 0),
			Open:                parseFloat(rk[1]),
			High:                parseFloat(rk[2]),
			Low:                 parseFloat(rk[3]),
			Close:               parseFloat(rk[4]),
			Volume:              parseFloat(rk[5]),
			CloseTime:           time.Unix(closeTimeUnix, 0),
			QuoteVolume:         parseFloat(rk[7]),
			Count:               int(rk[8].(float64)),
			TakerBuyVolume:      parseFloat(rk[9]),
			TakerBuyQuoteVolume: parseFloat(rk[10]),
			Ignore:              rk[11].(string),
		}
		klines = append(klines, kline)
	}
	return klines, nil
}

// получаем список всех торгуемых пар с Binance
func (w *Worker) fetchAllPairs() ([]string, error) {
	var info ExchangeInfo
	// запрос к binance для получения информации по парам
	if err := w.makeAPIRequest(ExchangeInfoAPIURL, &info); err != nil {
		return nil, err
	}

	// формируем списка пар для возврата
	pairs := make([]string, 0, len(info.Symbols))
	for _, symbol := range info.Symbols {
		pairs = append(pairs, symbol.Symbol)
	}
	return pairs, nil
}

// общая функция отправки запроса к апи и декодирования ответа
func (w *Worker) makeAPIRequest(url string, result interface{}) error {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	req.Header.Add("X-MBX-APIKEY", w.ApiKey) // Добавление ключа API в заголовки

	// отправка запроса
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close() // Закрытие тела ответа после завершения функции

	// проверка статуса ответа
	if res.StatusCode != 200 {
		return fmt.Errorf("error response from Binance API: %s", res.Status)
	}

	// декодирование ответа
	return json.NewDecoder(res.Body).Decode(result)
}

// преобразуем значение в float64
func parseFloat(value interface{}) float64 {
	strVal, ok := value.(string)
	if !ok {
		return 0
	}
	floatVal, err := strconv.ParseFloat(strVal, 64)
	if err != nil {
		return 0
	}
	return floatVal
}

// структура для информации о торгуемых парах
type ExchangeInfo struct {
	Symbols []SymbolInfo `json:"symbols"`
}

type SymbolInfo struct {
	Symbol string `json:"symbol"`
	Status string `json:"status"`
}
