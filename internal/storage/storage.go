package storage

import (
	"binance-parser/internal/models"
	"context"
	"gitlab.com/golight/orm/db/adapter"
	"gitlab.com/golight/orm/utils"
	"go.uber.org/zap"
)

type Storage struct {
	adapter *adapter.SQLAdapter
	logger  *zap.Logger
}

func NewStorage(adapter *adapter.SQLAdapter, logger *zap.Logger) *Storage {
	return &Storage{adapter: adapter, logger: logger}
}

func (s Storage) Save(ctx context.Context, klines []models.KLine) error {
	for _, kline := range klines {

		klineDTO := models.KLineDTO{
			Name:                kline.Name,
			OpenTime:            kline.OpenTime,
			Open:                kline.Open,
			High:                kline.High,
			Low:                 kline.Low,
			Close:               kline.Close,
			Volume:              kline.Volume,
			CloseTime:           kline.CloseTime,
			QuoteVolume:         kline.QuoteVolume,
			Count:               kline.Count,
			TakerBuyVolume:      kline.TakerBuyVolume,
			TakerBuyQuoteVolume: kline.TakerBuyQuoteVolume,
			Ignore:              kline.Ignore,
		}

		err := s.adapter.Create(ctx, &klineDTO)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Storage) GetInfo(symbol string) ([]models.KLineDTO, error) {
	// Инициализируем условие для выборки записей по символу
	condition := utils.Condition{
		Equal: map[string]interface{}{
			"name": symbol,
		},
	}
	// инициализируем массив для результатов
	var klines []models.KLineDTO

	// выполняем запрос к базе данных
	err := s.adapter.List(context.TODO(), &klines, "kline", condition)
	if err != nil {
		return nil, err
	}

	if len(klines) == 0 {
		s.logger.Info("no any data", zap.String("symbol", symbol))
	}
	return klines, nil
}
