package klines

import (
	"binance-parser/internal/models"
	"context"
	"fmt"
	"github.com/cinar/indicator"
	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/components"
	"github.com/go-echarts/go-echarts/v2/opts"
	"github.com/go-echarts/go-echarts/v2/types"
	"github.com/volatiletech/null/v8"
	"gitlab.com/golight/orm/db/adapter"
	"gitlab.com/golight/orm/utils"
	"go.uber.org/zap"
	"io"
	"os"
	"strconv"
	"sync"
	"time"
)

const (
	binanceAPIKey    = "CKbKRM7L9FcUK6rFvqfcliHvOAEonXLE3oWlW3efQi0GWdCyIZokuUz7vo7dArrK"
	binanceSecretKey = "H5aDJgL5Up8DXS59WmSAbl6DEQVo0lm0GDWRiYF1BJb06eO9ejpkFuV5s4qmSV7i"
)

func NewKlineService(adapter *adapter.SQLAdapter, logger *zap.Logger) *KlineService {
	return &KlineService{adapter: adapter, logger: logger}
}

type KlineService struct {
	adapter *adapter.SQLAdapter
	logger  *zap.Logger
}

type KlineServicer interface {
	CreateGraphs(symbols, periods []string)
	getKline(symbol, period string) ([]*models.KLineDTO, error)
	klineDataZoomInside(symbol, period string) []components.Charter
}

var wg sync.WaitGroup

type klineData struct {
	date string
	data [4]float64
}

var lastStochD = make(map[string]float64, 2)
var mu = sync.Mutex{}

func (K *KlineService) klineDataZoomInside(symbol, period string) []components.Charter {
	data, err := K.getKline(symbol, period)
	if err != nil {
		return nil
	}
	kd := Kline(data)
	kline := charts.NewKLine()

	x := make([]string, 0)
	y := make([]opts.KlineData, 0)
	for i := 0; i < len(kd); i++ {
		x = append(x, kd[i].date)
		y = append(y, opts.KlineData{Value: kd[i].data})
	}

	kline.SetGlobalOptions(
		charts.WithTitleOpts(opts.Title{
			Title: fmt.Sprintf("%s %s", symbol, period),
		}),
		charts.WithInitializationOpts(opts.Initialization{Theme: types.ThemeWalden}),
		charts.WithXAxisOpts(opts.XAxis{
			SplitNumber: 20,
		}),
		charts.WithYAxisOpts(opts.YAxis{
			Scale: true,
		}),
		charts.WithDataZoomOpts(opts.DataZoom{
			Type:       "inside",
			Start:      50,
			End:        100,
			XAxisIndex: []int{0},
		}),
		charts.WithDataZoomOpts(opts.DataZoom{
			Type:       "slider",
			Start:      50,
			End:        100,
			XAxisIndex: []int{0},
		}),
		charts.WithTooltipOpts(opts.Tooltip{Show: true, Trigger: "axis"}),
	)

	kline.SetXAxis(x).AddSeries("kline", y)
	var k, d []float64

	var high, low, closing []float64
	var unixTime []string
	for i := range data {
		closePrice := data[i].Close
		closing = append(closing, closePrice)
		lowPrice := data[i].Low
		low = append(low, lowPrice)
		highPrice := data[i].High
		high = append(high, highPrice)
		t := data[i].CloseTime
		//// Преобразование миллисекунд в секунды и наносекунды
		//seconds := millis / 1000
		//nanos := (millis % 1000) * 1000000
		//// Создание нового объекта времени
		//t := time.Unix(seconds, nanos)

		if i == len(data)-1 {
			t = time.Now()
		}

		unixTime = append(unixTime, t.Format("2006-01-02 15:04:05"))
	}
	k, d = indicator.StochasticOscillator(high, low, closing)

	line := charts.NewLine()
	line.SetGlobalOptions(
		charts.WithTitleOpts(opts.Title{Title: "Stochastic RSI", Subtitle: "This is the subtitle."}),
		charts.WithDataZoomOpts(opts.DataZoom{
			Type:       "inside",
			Start:      50,
			End:        100,
			XAxisIndex: []int{0},
		}),
		charts.WithDataZoomOpts(opts.DataZoom{
			Type:       "slider",
			Start:      50,
			End:        100,
			XAxisIndex: []int{0},
		}),
		charts.WithTooltipOpts(opts.Tooltip{Show: true, Trigger: "axis"}),
	)

	mu.Lock()
	lastStochD[fmt.Sprintf("%s_%s", symbol, period)] = d[len(d)-1]
	mu.Unlock()

	line.SetXAxis(unixTime).
		AddSeries("K", getLineData(k)).
		AddSeries("D", getLineData(d)).
		SetSeriesOptions(charts.WithLineChartOpts(
			opts.LineChart{Smooth: false, ShowSymbol: true, SymbolSize: 8},
		))

	return []components.Charter{kline, line}
}

func getLineData(data []float64) []opts.LineData {
	var err error

	items := make([]opts.LineData, 0, len(data))
	for i := range data {
		if data[i] < 0 {
			fmt.Println(data[i])
			//data[i] = 0

		}
		item := fmt.Sprintf("%.11f", data[i])
		data[i], err = strconv.ParseFloat(item, 64)

		if err != nil {
			fmt.Println(err)
			continue
		}
		items = append(items, opts.LineData{Value: data[i]})
	}
	return items
}

func (K *KlineService) CreateGraphs(symbols, periods []string) { // "1m", "15m", "1h", "4h", "1d", "1w"
	for _, symbol := range symbols {
		wg.Add(1)
		go func(symbol string) {
			defer wg.Done()
			page := components.NewPage()
			page.SetLayout(components.PageFlexLayout)
			for _, period := range periods {
				page.AddCharts(K.klineDataZoomInside(symbol, period)...)
			}
			page.AddCharts(pieBase(symbol, periods...))

			err := os.Mkdir("html", os.ModePerm)
			if err != nil && !os.IsExist(err) {
				fmt.Println(err)
				return
			}

			f, err := os.Create(fmt.Sprintf("html/%s.html", symbol))
			defer f.Close()
			if err != nil {
				panic(err)

			}
			err = page.Render(io.MultiWriter(f))
			if err != nil {
				fmt.Println(err)
				return
			}
		}(symbol)
	}
	wg.Wait()
}

func createPieItems(symbol string, periods ...string) []opts.PieData {
	items := make([]opts.PieData, 0)
	long := float64(len(periods) * 100)
	var short float64
	for _, period := range periods {
		if _, ok := lastStochD[fmt.Sprintf("%s_%s", symbol, period)]; !ok {
			long = 0
			return nil
		}
		short += lastStochD[fmt.Sprintf("%s_%s", symbol, period)]
	}
	long -= short
	items = append(items, opts.PieData{Name: "long strength", Value: long})
	items = append(items, opts.PieData{Name: "short strength", Value: short})

	return items
}

func pieBase(symbol string, periods ...string) components.Charter {
	pie := charts.NewPie()
	pie.SetGlobalOptions(
		charts.WithTitleOpts(opts.Title{Title: "long short probability"}),
	)

	pie.AddSeries("pie", createPieItems(symbol, periods...)).
		SetSeriesOptions(charts.WithLabelOpts(
			opts.Label{
				Show:      true,
				Formatter: "{b}: {c}",
			}),
		)
	return pie
}

type NullTime struct {
	null.Time
}

// getKline получает данные о котировках с биржи
func (K *KlineService) getKline(symbol, period string) ([]*models.KLineDTO, error) {
	//client := binance.NewClient(binanceAPIKey, binanceSecretKey)
	var Klines []*models.KLineDTO
	targetSymbol := symbol

	//targetInterval := period /// case sensitive, 1D != 1d or 30M != 30m

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err := K.adapter.List(ctx, &Klines, "kline", utils.Condition{
		Equal: map[string]interface{}{
			"name": targetSymbol,
		},
	})
	if err != nil {
		return nil, err
	}
	return Klines, err
	//return client.NewKlinesService().Symbol(targetSymbol).Interval(targetInterval).Do(ctx)
}

// Kline преобразует данные о котировках в формате binance.Kline в формат klineData
func Kline(data []*models.KLineDTO) []klineData {
	kd := make([]klineData, 1, len(data))

	for i := 0; i < len(data); i++ {
		t := data[i].CloseTime

		// Преобразование миллисекунд в секунды и наносекунды
		//seconds := millis / 1000
		//nanos := (millis % 1000) * 1000000
		//// Создание нового объекта времени
		//t := time.Unix(seconds, nanos)

		if i == len(data)-1 {
			t = time.Now()
		}

		closePrice := data[i].Close
		openPrice := data[i].Open
		lowPrice := data[i].Low
		highPrice := data[i].High

		kd = append(kd, klineData{
			date: t.Format(time.RFC3339),
			data: [4]float64{closePrice, openPrice, lowPrice, highPrice},
		})
	}

	return kd
}
