package service

import (
	"binance-parser/internal/models"
	"context"
)

type Servicer interface {
	StartParsing(ctx context.Context)
	StopParsing()
	GetInfo(symbol string) ([]models.KLineDTO, error)
}
