package service

import (
	"binance-parser/internal/models"
	"binance-parser/internal/storage"
	"binance-parser/internal/worker"
	"context"
)

type Service struct {
	Storage storage.Storager
	Worker  *worker.Worker
}

func NewService(storage *storage.Storage, worker *worker.Worker) *Service {
	return &Service{Storage: storage, Worker: worker}
}

func (s *Service) StartParsing(ctx context.Context) {
	if s.Worker.IsRunning {
		return
	}
	s.Worker.IsRunning = true
	s.Worker.StopChan = make(chan bool)

	go s.Worker.Run(ctx, s.Worker.StopChan)
}

func (s *Service) StopParsing() {
	if !s.Worker.IsRunning {
		return
	}
	s.Worker.StopChan <- true
	close(s.Worker.StopChan)

	s.Worker.IsRunning = false
}

func (s *Service) GetInfo(symbol string) ([]models.KLineDTO, error) {
	return s.Storage.GetInfo(symbol)
}
