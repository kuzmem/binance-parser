package main

import (
	"binance-parser/config"
	"binance-parser/run"
	"fmt"
	"github.com/joho/godotenv"
	"go.uber.org/zap"
	"os"
)

// @title           binance parser
// @version         v.0.0.1
// @description     k-line binance parser
// @host      localhost:8080
func main() {
	err := godotenv.Load()
	conf := config.NewAppConf()
	logger, err := zap.NewDevelopment()
	if err != nil {
		fmt.Printf("Cannot initialize zap logger: %v", err)
		os.Exit(1)
	}
	defer logger.Sync() // очистка логгера перед завершением программы
	if err != nil {

	}
	app := run.NewApp(conf, logger)
	// инициализируем и запуcкаем приложение
	exitCode := app.Bootstrap().Run()
	os.Exit(exitCode)
}
