// Code generated by swaggo/swag. DO NOT EDIT.

package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "contact": {},
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/getinfo": {
            "get": {
                "description": "get pair info",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "parser"
                ],
                "summary": "GetInfo",
                "parameters": [
                    {
                        "description": "JSON object",
                        "name": "input",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/models.RequestData"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/models.KLineDTO"
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error"
                    }
                }
            }
        },
        "/start": {
            "post": {
                "description": "Parsing k-line pairs",
                "tags": [
                    "parser"
                ],
                "summary": "StartParsing",
                "responses": {
                    "200": {
                        "description": "OK"
                    }
                }
            }
        },
        "/stop": {
            "post": {
                "description": "Stop parsing k-line pairs",
                "tags": [
                    "parser"
                ],
                "summary": "StopParsing",
                "responses": {
                    "200": {
                        "description": "OK"
                    }
                }
            }
        }
    },
    "definitions": {
        "models.KLineDTO": {
            "type": "object",
            "properties": {
                "close": {
                    "type": "number"
                },
                "close_time": {
                    "type": "string"
                },
                "count": {
                    "type": "integer"
                },
                "high": {
                    "type": "number"
                },
                "id": {
                    "type": "integer"
                },
                "ignore": {
                    "type": "string"
                },
                "low": {
                    "type": "number"
                },
                "name": {
                    "type": "string"
                },
                "open": {
                    "type": "number"
                },
                "open_time": {
                    "type": "string"
                },
                "quote_volume": {
                    "type": "number"
                },
                "taker_buy_quote_volume": {
                    "type": "number"
                },
                "taker_buy_volume": {
                    "type": "number"
                },
                "volume": {
                    "type": "number"
                }
            }
        },
        "models.RequestData": {
            "type": "object",
            "properties": {
                "symbol": {
                    "type": "string"
                }
            }
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "v.0.0.1",
	Host:             "localhost:8080",
	BasePath:         "",
	Schemes:          []string{},
	Title:            "binance parser",
	Description:      "k-line binance parser",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
	LeftDelim:        "{{",
	RightDelim:       "}}",
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
